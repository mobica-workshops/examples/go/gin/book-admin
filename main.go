package main

import "gitlab.com/mobica-workshops/examples/go/gin/book-admin/cmd"

func main() {
	cmd.Execute()
}
