package services

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/repository"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type AdminService interface {
	CreateAdmin(c *gin.Context)
	GetAdmins(c *gin.Context)
	GetAdmin(c *gin.Context)
	UpdateAdmin(c *gin.Context)
	DeleteAdmin(c *gin.Context)
}

func NewAdminService(db *mongo.Database) *Service {
	repo := repository.CreateAdminRepository(db)

	return &Service{repo}
}

func (s *Service) CreateAdmin(c *gin.Context) {
	input := models.CreateAdminInput{}

	if c.BindJSON(&input) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with creating admin", []string{"Provide relevant fields"}))
		c.Abort()

		return
	}

	admin, err := models.NewAdmin(&input)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with creating admin", []string{err.Error()}))
		c.Abort()

		return
	}

	admin, error := s.repository.Create(admin)

	if error != nil {
		c.JSON(error.Code, models.GenerateErrorResponse(error.Code, "Problem with creating admin", []string{error.Message}))
		c.Abort()
		return
	}

	c.JSON(http.StatusCreated, models.GenerateResponse(http.StatusCreated, fmt.Sprintf("Admin: %s created", admin.ID.Hex()), &models.NewAdminResponse{ID: admin.ID.Hex()}))
}

func (s *Service) GetAdmins(c *gin.Context) {
	pagination := models.PaginationInput{}

	if err := c.BindQuery(&pagination); err != nil {
		c.JSON(http.StatusNotFound, models.GenerateErrorResponse(http.StatusNotFound, "Problem with finding admins", []string{"Provide relevant pagination fields"}))
		c.Abort()

		return
	}

	admins, count, error := s.repository.GetBatch(&pagination)

	if error != nil {
		c.JSON(error.Code, models.GenerateErrorResponse(error.Code, "Problem with finding admins", []string{error.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, fmt.Sprintf("admins - skip: %d; limit: %d", pagination.Offset, pagination.Limit), &models.Pagination{Count: count, Skip: pagination.Offset, Limit: pagination.Limit, Results: &admins}))
}

func (s *Service) GetAdmin(c *gin.Context) {
	urlParam := models.AdminID{}

	if c.ShouldBindUri(&urlParam) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with finding Admin", []string{"Provide existing admin id"}))
		c.Abort()

		return
	}

	id, err := primitive.ObjectIDFromHex(urlParam.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with finding Admin", []string{"Provded id has incorrect format"}))
		c.Abort()

		return
	}

	admin, error := s.repository.FindOne(id)

	if error != nil {
		c.JSON(error.Code, models.GenerateErrorResponse(error.Code, "Problem with finding Admin", []string{error.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, fmt.Sprintf("book-admin admin id:%s found", urlParam.ID), &admin))
}

func (s *Service) UpdateAdmin(c *gin.Context) {
	urlParam := models.AdminID{}

	if err := c.ShouldBindUri(&urlParam); err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating Admin", []string{"Provide existing admin id"}))
		c.Abort()

		return
	}

	id, err := primitive.ObjectIDFromHex(urlParam.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating Admin", []string{"Provded id has incorrect format"}))
		c.Abort()

		return
	}

	input := models.UpdateAdminInput{}

	if c.BindJSON(&input) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating admin", []string{"Provide relevant fields"}))
		c.Abort()

		return
	}

	if input.Password != "" {
		input.PasswordHash, err = models.HashPassword(input.Password)

		if err != nil {
			c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating Admin", []string{"could not process input data"}))
			c.Abort()

			return
		}
	}

	output, error := s.repository.Update(id, &input)

	if error != nil {
		c.JSON(error.Code, models.GenerateErrorResponse(error.Code, "Problem with updating Admin", []string{error.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusAccepted, models.GenerateResponse(http.StatusAccepted, fmt.Sprintf("book-admin admin id:%s updated", urlParam.ID), &output))
}

func (s *Service) DeleteAdmin(c *gin.Context) {
	urlParam := models.AdminID{}

	if c.ShouldBindUri(&urlParam) != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with finding Admin", []string{"Provide existing admin id"}))
		c.Abort()

		return
	}

	id, err := primitive.ObjectIDFromHex(urlParam.ID)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(http.StatusBadRequest, "Problem with updating Admin", []string{"Provded id has incorrect format"}))
		c.Abort()

		return
	}

	if err := s.repository.Delete(id); err != nil {
		c.JSON(http.StatusBadRequest, models.GenerateErrorResponse(err.Code, "Problem with updating Admin", []string{err.Message}))
		c.Abort()

		return
	}

	c.JSON(http.StatusAccepted, models.GenerateResponse(http.StatusAccepted, "successfully deleted", nil))
}
