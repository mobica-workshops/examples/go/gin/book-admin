package services

import (
	"net/http"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/repository"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type HealthService interface {
	CheckHealth(c *gin.Context)
}

func NewHealthService(db *mongo.Database) *Service {
	repo := repository.CreateAdminRepository(db)
	return &Service{repo}
}

func (s *Service) CheckHealth(c *gin.Context) {
	dbHealth := s.repository.HealthCheck()

	c.JSON(http.StatusOK, models.GenerateResponse(http.StatusOK, "book-admin api health", &models.Health{Alive: true, Mongo: dbHealth}))
}
