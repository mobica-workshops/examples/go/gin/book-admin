package services

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/repository"

	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type MockedRepository struct {
	mock.Mock
}

func (r *MockedRepository) FindOne(id primitive.ObjectID) (*models.Admin, *repository.RequestError) {
	args := r.Called(id)
	return args.Get(0).(*models.Admin), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) GetBatch(input *models.PaginationInput) (*[]models.Admin, int64, *repository.RequestError) {
	args := r.Called(input)
	return args.Get(0).(*[]models.Admin), args.Get(1).(int64), args.Get(2).(*repository.RequestError)
}

func (r *MockedRepository) Create(input *models.Admin) (*models.Admin, *repository.RequestError) {
	args := r.Called(input)
	return args.Get(0).(*models.Admin), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) Update(id primitive.ObjectID, input *models.UpdateAdminInput) (*models.Admin, *repository.RequestError) {
	args := r.Called(id, input)
	return args.Get(0).(*models.Admin), args.Get(1).(*repository.RequestError)
}

func (r *MockedRepository) Delete(id primitive.ObjectID) *repository.RequestError {
	args := r.Called(id)
	return args.Get(0).(*repository.RequestError)
}

func (r *MockedRepository) HealthCheck() bool {
	args := r.Called()
	return args.Bool(0)
}
