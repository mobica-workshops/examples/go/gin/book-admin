package services

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/repository"
)

type Service struct {
	repository repository.AdminRepository
}
