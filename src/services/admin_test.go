package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/repository"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

var adminBody = []byte(`{
		"email": "email@example.com",
		"firstName": "John",
		"lastName": "Doe",
		"password": "pass123",
		"version": 1
	  }`)

var adminObject = models.Admin{Email: "email@example.com", FirstName: "John", LastName: "Doe", PasswordHash: "$2a$14$l2c4QmWWQ.RzuDy85JXbx.jicU2pok/ca/xdAUR1JGyXh0vTNm48O", Version: 1}

func TestCreateAdmin(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(adminBody))
	output := adminObject
	output.ID = primitive.NewObjectID()

	mockedRepo.On("Create", mock.Anything).Return(&output, (*repository.RequestError)(nil))

	s.CreateAdmin(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusCreated, Message: fmt.Sprintf("Admin: %s created", output.ID.Hex()), Content: map[string]interface{}{"id": output.ID.Hex()}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusCreated, w.Code)
}

func TestCreateAdminRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(adminBody))
	mockedRepo.On("Create", mock.Anything).Return((*models.Admin)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.CreateAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with creating admin", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateAdmin(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	output := adminObject
	output.ID = primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(adminBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: output.ID.Hex()})

	mockedRepo.On("Update", output.ID, mock.Anything).Return(&output, (*repository.RequestError)(nil))

	s.UpdateAdmin(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusAccepted, Message: fmt.Sprintf("book-admin admin id:%s updated", output.ID.Hex())}

	assert.EqualValues(t, expected.Message, got.Message)
	assert.EqualValues(t, http.StatusAccepted, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateAdminMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(adminBody))

	s.UpdateAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with updating Admin", Errors: []string{"Provide existing admin id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateAdminIncorrectId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(adminBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "zzzzzzzzzzzzzzzzzzzzzzzz"})

	s.UpdateAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with updating Admin", Errors: []string{"Provded id has incorrect format"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestUpdateAdminRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPut, "/", bytes.NewBuffer(adminBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: id.Hex()})
	mockedRepo.On("Update", id, mock.Anything).Return((*models.Admin)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.UpdateAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with updating Admin", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteAdmin(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: id.Hex()})
	mockedRepo.On("Delete", id).Return((*repository.RequestError)(nil))

	s.DeleteAdmin(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusAccepted, Message: "successfully deleted", Content: interface{}(nil)}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusAccepted, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteAdminMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))

	s.DeleteAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with finding Admin", Errors: []string{"Provide existing admin id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteAdminIncorrectId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "zzzzzzzzzzzzzzzzzzzzzzzz"})

	s.DeleteAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with updating Admin", Errors: []string{"Provded id has incorrect format"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestDeleteAdminRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(adminBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: id.Hex()})
	mockedRepo.On("Delete", id).Return(&repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.DeleteAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with updating Admin", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdmin(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	output := adminObject
	output.ID = primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: output.ID.Hex()})

	mockedRepo.On("FindOne", output.ID).Return(&output, (*repository.RequestError)(nil))

	s.GetAdmin(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: fmt.Sprintf("book-admin admin id:%s found", output.ID.Hex())}

	assert.EqualValues(t, expected.Message, got.Message)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdminIncorrectId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: "zzzzzzzzzzzzzzzzzzzzzzzz"})

	s.GetAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with finding Admin", Errors: []string{"Provded id has incorrect format"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
}

func TestGetAdminMissingId(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer([]byte("{}")))

	s.GetAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusBadRequest, Message: "Problem with finding Admin", Errors: []string{"Provide existing admin id"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusBadRequest, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdminRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodPost, "/", bytes.NewBuffer(adminBody))
	c.Params = append(c.Params, gin.Param{Key: "id", Value: id.Hex()})
	mockedRepo.On("FindOne", id).Return((*models.Admin)(nil), &repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.GetAdmin(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with finding Admin", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdmins(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/?limit=1&skip=1", bytes.NewBuffer([]byte("{}")))
	pagination := models.PaginationInput{Limit: 1, Offset: 1}
	bookOutput := adminObject
	bookOutput.ID = id
	mockedRepo.On("GetBatch", &pagination).Return(&[]models.Admin{bookOutput}, int64(1), (*repository.RequestError)(nil))

	s.GetAdmins(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "admins - skip: 1; limit: 1"}

	assert.EqualValues(t, expected.Message, got.Message)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdminsDefaultPagination(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}
	id := primitive.NewObjectID()
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	pagination := models.PaginationInput{Limit: 10, Offset: 0}
	bookOutput := adminObject
	bookOutput.ID = id
	mockedRepo.On("GetBatch", &pagination).Return(&[]models.Admin{bookOutput}, int64(1), (*repository.RequestError)(nil))

	s.GetAdmins(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "admins - skip: 0; limit: 10",}

	assert.EqualValues(t, expected.Message, got.Message)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestGetAdminsRepositoryError(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	pagination := models.PaginationInput{Limit: 10, Offset: 0}
	mockedRepo.On("GetBatch", &pagination).Return((*[]models.Admin)(nil), int64(0), &repository.RequestError{Code: http.StatusNotFound, Message: "not found"})

	s.GetAdmins(c)

	got := models.ErrorResponse{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.ErrorResponse{Status: http.StatusNotFound, Message: "Problem with finding admins", Errors: []string{"not found"}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusNotFound, w.Code)
	mockedRepo.AssertExpectations(t)
}
