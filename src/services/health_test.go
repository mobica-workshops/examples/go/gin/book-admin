package services

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestCheckHealth(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	mockedRepo.On("HealthCheck").Return(true)

	s.CheckHealth(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "book-admin api health", Content: map[string]interface{}{"alive": true, "mongo": true}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}

func TestCheckHealthDBIssue(t *testing.T) {
	mockedRepo := new(MockedRepository)
	s := Service{repository: mockedRepo}

	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Request, _ = http.NewRequest(http.MethodGet, "/", bytes.NewBuffer([]byte("{}")))
	mockedRepo.On("HealthCheck").Return(false)

	s.CheckHealth(c)

	got := models.Response{}
	json.Unmarshal(w.Body.Bytes(), &got)
	expected := models.Response{Status: http.StatusOK, Message: "book-admin api health", Content: map[string]interface{}{"alive": true, "mongo": false}}

	assert.EqualValues(t, expected, got)
	assert.EqualValues(t, http.StatusOK, w.Code)
	mockedRepo.AssertExpectations(t)
}
