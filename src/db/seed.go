package db

import (
	"context"
	"log"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/config"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"
)

func Seed(config *config.Config) {
	database := ConnectDB(config.MongoDb.Uri, config.MongoDb.Database)
	log.Println("Seed started")

	if database.Collection("admin").Drop(context.Background()) != nil {
		log.Panic("Could not remove data! Seed aborted.")
	}

	log.Println("Dropping all tables completed")

	admin, err := models.NewAdmin(&models.CreateAdminInput{Email: "piotr.dudek@mobica.com", FirstName: "Piotr", LastName: "Dudek", Password: "#superStrongPass1234"})
	if err != nil {
		log.Println("Could not seed database.")
		return
	}

	database.Collection("admin").InsertOne(context.Background(), admin)

	log.Println("Seed successfully completed")

	return
}
