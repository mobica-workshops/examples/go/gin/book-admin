package db

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectDB(uri string, database string) *mongo.Database {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(uri))
	if err != nil {
		log.Fatal(err)
	}

	return client.Database(database)
}

func DisconnectDB(db *mongo.Database) {
	if db.Client().Disconnect(context.Background()) != nil {
		panic("Failed to kill connection from database")
	}
}
