package repository

import (
	// "log"
	// "net/http"
	// "reflect"

	"context"
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"reflect"
	"time"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type AdminRepository interface {
	FindOne(id primitive.ObjectID) (*models.Admin, *RequestError)
	GetBatch(input *models.PaginationInput) (*[]models.Admin, int64, *RequestError)
	Create(input *models.Admin) (*models.Admin, *RequestError)
	Update(id primitive.ObjectID, input *models.UpdateAdminInput) (*models.Admin, *RequestError)
	Delete(id primitive.ObjectID) *RequestError
	HealthCheck() bool
}

func CreateAdminRepository(db *mongo.Database) AdminRepository {
	repo := Repository{db, db.Collection("admin")}

	indexName := repo.RegisterIndex("email", true)
	log.Println("Successfully registered index", indexName, "for collection", "admin")

	return &repo
}

func (r *Repository) FindOne(id primitive.ObjectID) (*models.Admin, *RequestError) {
	var entity models.Admin

	if err := r.Collection.FindOne(context.Background(), &models.Admin{ID: id}).Decode(&entity); err != nil {
		switch err {
		case mongo.ErrNoDocuments:
			return nil, &RequestError{Error: err, Message: "Error occurred while reading data", Code: http.StatusNotFound}
		default:
			log.Println("Error while decode to go struct:", err)
			return nil, &RequestError{Error: err, Message: "Error occurred while reading data", Code: http.StatusInternalServerError}
		}
	}

	return &entity, nil
}

func (r *Repository) GetBatch(input *models.PaginationInput) (*[]models.Admin, int64, *RequestError) {
	var batch []models.Admin
	var count int64

	filter := bson.M{}
	if input.Email != "" {
		filter["email"] = input.Email
	}

	countOptions := options.CountOptions{}
	count, err := r.Collection.CountDocuments(context.Background(), filter, &countOptions)
	if err != nil {
		log.Println("Error while counting collection:", err)
		return nil, 0, &RequestError{Error: err, Message: "Error occurred while reading data", Code: http.StatusInternalServerError}
	}

	findOptions := options.FindOptions{
		Skip:  &input.Offset,
		Limit: &input.Limit,
		Sort: bson.M{
			"_id": -1,
		},
	}
	cursor, err := r.Collection.Find(context.Background(), filter, &findOptions)
	if err != nil {
		log.Println("Error while quering collection:", err)

		return nil, 0, &RequestError{Error: err, Message: "Error occurred while reading data", Code: http.StatusInternalServerError}
	}
	err = cursor.All(context.Background(), &batch)
	if err != nil {
		log.Println(err.Error())
		return nil, 0, &RequestError{Error: err, Message: "Error occurred while reading data", Code: http.StatusInternalServerError}
	}

	return &batch, count, nil
}

func (r *Repository) Create(input *models.Admin) (*models.Admin, *RequestError) {
	result, err := r.Collection.InsertOne(context.Background(), &input)
	if err != nil {
		switch err.(type) {
		case mongo.WriteException:
			return nil, &RequestError{Error: err, Message: "Username or email already exists", Code: http.StatusConflict}
		default:
			return nil, &RequestError{Error: err, Message: "Could not execute db query", Code: http.StatusInternalServerError}
		}
	}

	input.ID = result.InsertedID.(primitive.ObjectID)

	return input, nil
}

func (r *Repository) Update(id primitive.ObjectID, input *models.UpdateAdminInput) (*models.Admin, *RequestError) {
	bytes, err := json.Marshal(input)
	if err != nil {
		return nil, &RequestError{Error: err, Message: "Could not process data", Code: http.StatusInternalServerError}
	}

	var updateData map[string]interface{}

	if json.Unmarshal(bytes, &updateData) != nil {
		return nil, &RequestError{Error: err, Message: "Could not process data", Code: http.StatusInternalServerError}
	}
	
	if _, ok := updateData["password"]; ok {
		delete(updateData, "password")
	}

	update := bson.M{
		"$set": updateData,
	}

	updated := models.Admin{}
	
	error := r.Collection.FindOneAndUpdate(context.Background(), &models.Admin{ID: id}, update, options.FindOneAndUpdate().SetReturnDocument(options.After)).Decode(&updated)

	if error != nil {
		log.Printf(error.Error())
		return nil, &RequestError{Error: error, Message: "Could not update document", Code: http.StatusInternalServerError}
	}
	
	if reflect.ValueOf(updated).IsZero() {
		return nil, &RequestError{Error: errors.New("Document not found"), Message: "Could not update document", Code: http.StatusNotFound}
	}

	return &updated, nil
}

func (r *Repository) Delete(id primitive.ObjectID) *RequestError {
	result, err := r.Collection.DeleteOne(context.Background(), models.Admin{ID: id})

	if err != nil {
		return &RequestError{Error: err, Message: "Could not execute db query", Code: http.StatusInternalServerError}
	} else if result.DeletedCount < 1 {
		return &RequestError{Error: errors.New("Could not found document in db"), Message: "Record not found", Code: http.StatusNotFound}
	}

	return nil
}

func (r *Repository) HealthCheck() bool {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	response := true

	defer cancel()

	err := r.db.Client().Ping(ctx, nil)
	if err != nil {
		response = false
	}

	return response
}
