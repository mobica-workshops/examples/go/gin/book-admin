package repository

import (
	"testing"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/models"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
)

func TestFindOne(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		adminMock := models.Admin{
			ID:        primitive.NewObjectID(),
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(1, "dbName.collName", mtest.FirstBatch, bson.D{
			{Key: "_id", Value: adminMock.ID},
			{Key: "firstName", Value: adminMock.FirstName},
			{Key: "email", Value: adminMock.Email},
		}))
		r := Repository{Collection: mt.Coll}
		userResponse, err := r.FindOne(adminMock.ID)

		assert.Nil(t, err)
		assert.Equal(t, &adminMock, userResponse)
	})
}

func TestFindOneFailDecode(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		mt.AddMockResponses(bson.D{{"ok", 0}})
		r := Repository{Collection: mt.Coll}
		_, err := r.FindOne(primitive.NewObjectID())
		assert.Equal(t, err.Code, 500)
	})
}

func TestFindOneNotFound(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		mt.AddMockResponses(mtest.CreateCursorResponse(
			0,
			"dbName.collName",
			mtest.FirstBatch,
		))
		r := Repository{Collection: mt.Coll}
		_, err := r.FindOne(primitive.NewObjectID())
		assert.Equal(t, err.Code, 404)
	})
}

func TestGetBatch(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		adminMock := models.Admin{
			ID:        primitive.NewObjectID(),
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(
			1,
			"dbName.collName",
			mtest.FirstBatch, bson.D{
				bson.E{Key: "n", Value: 5},
			}), mtest.CreateCursorResponse(1, "dbName.collName", mtest.FirstBatch, bson.D{
			{Key: "_id", Value: adminMock.ID},
			{Key: "firstName", Value: adminMock.FirstName},
			{Key: "email", Value: adminMock.Email},
		}), mtest.CreateCursorResponse(0, "dbName.collName", mtest.NextBatch))
		r := Repository{Collection: mt.Coll}
		input := models.PaginationInput{Limit: 1, Offset: 1}
		response, count, err := r.GetBatch(&input)

		assert.Nil(t, err)
		assert.Equal(t, int(count), int(5))
		assert.Equal(t, adminMock.FirstName, (*response)[0].FirstName)
	})
}

func TestGetBatchCountError(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		mt.AddMockResponses(bson.D{{Key: "ok", Value: 0}})
		r := Repository{Collection: mt.Coll}
		input := models.PaginationInput{Limit: 1, Offset: 1}
		_, count, err := r.GetBatch(&input)

		assert.Equal(t, int(count), int(0))
		assert.Equal(t, err.Error.Error(), "command failed")
	})
}

func TestGetBatchQueryIssue(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		mt.AddMockResponses(mtest.CreateCursorResponse(
			1,
			"dbName.collName",
			mtest.FirstBatch, bson.D{
				bson.E{Key: "n", Value: 5},
			}), bson.D{{Key: "ok", Value: 0}})
		r := Repository{Collection: mt.Coll}
		input := models.PaginationInput{Limit: 1, Offset: 1}
		_, count, err := r.GetBatch(&input)

		assert.Equal(t, int(count), int(0))
		assert.Equal(t, 500, err.Code)
	})
}

func TestCreate(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		adminMock := models.Admin{
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		adminReturnMock := models.Admin{
			ID:        primitive.NewObjectID(),
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		mt.AddMockResponses(mtest.CreateCursorResponse(1, "dbName.collName", mtest.FirstBatch, bson.D{
			{Key: "_id", Value: adminReturnMock.ID},
			{Key: "firstName", Value: adminMock.FirstName},
			{Key: "email", Value: adminMock.Email},
		}))
		r := Repository{Collection: mt.Coll}
		userResponse, err := r.Create(&adminReturnMock)

		assert.Nil(t, err)
		assert.Equal(t, adminMock.FirstName, userResponse.FirstName)
		assert.Equal(t, adminMock.Email, userResponse.Email)
	})
}

func TestCreateDuplicate(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		adminMock := models.Admin{
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		mt.AddMockResponses(mtest.CreateWriteErrorsResponse(mtest.WriteError{
			Index:   1,
			Code:    11000,
			Message: "duplicate key error",
		}))
		r := Repository{Collection: mt.Coll}
		_, err := r.Create(&adminMock)

		assert.Equal(t, "Username or email already exists", err.Message)
	})
}

func TestCreateError(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		adminMock := models.Admin{
			ID:        primitive.NewObjectID(),
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		mt.AddMockResponses(bson.D{{Key: "ok", Value: 0}})
		r := Repository{Collection: mt.Coll}
		_, err := r.Create(&adminMock)

		assert.Equal(t, "Could not execute db query", err.Message)
	})
}

func TestUpdate(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		adminMock := models.Admin{
			ID:        primitive.NewObjectID(),
			FirstName: "john",
			Email:     "john.doe@test.com",
		}

		update := models.UpdateAdminInput{
			FirstName: "Giorgio",
			Password:  "test123",
		}

		mt.AddMockResponses(bson.D{
			{Key: "ok", Value: 1},
			{Key: "value", Value: bson.D{
				{Key: "_id", Value: adminMock.ID},
				{Key: "firstName", Value: update.FirstName},
				{Key: "email", Value: adminMock.Email},
			}},
		})
		r := Repository{Collection: mt.Coll}
		userResponse, err := r.Update(adminMock.ID, &update)

		assert.Nil(t, err)
		assert.Equal(t, adminMock.Email, userResponse.Email)
		assert.Equal(t, update.FirstName, userResponse.FirstName)
	})
}

func TestUpdateError(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("fail", func(mt *mtest.T) {
		update := models.UpdateAdminInput{
			FirstName: "Giorgio2",
		}

		mt.AddMockResponses(bson.D{
			{Key: "ok", Value: 0},
			{Key: "value", Value: bson.D{{Key: "ok", Value: 0}}},
		})
		r := Repository{Collection: mt.Coll}
		_, err := r.Update(primitive.NewObjectID(), &update)

		assert.Equal(t, 500, err.Code)
		assert.Equal(t, "Could not update document", err.Message)
	})
}

func TestDelete(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		mt.AddMockResponses(bson.D{
			{Key: "ok", Value: 1},
			{Key: "acknowledged", Value: true},
			{Key: "n", Value: 1},
		})
		r := Repository{Collection: mt.Coll}
		err := r.Delete(primitive.NewObjectID())

		assert.Nil(t, err)
	})
}

func TestDeleteNotFound(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		mt.AddMockResponses(bson.D{
			{Key: "ok", Value: 1},
			{Key: "acknowledged", Value: true},
			{Key: "n", Value: 0},
		})
		r := Repository{Collection: mt.Coll}
		err := r.Delete(primitive.NewObjectID())

		assert.Equal(t, 404, err.Code)
		assert.Equal(t, "Record not found", err.Message)
	})
}

func TestDeleteError(t *testing.T) {
	mt := mtest.New(t, mtest.NewOptions().ClientType(mtest.Mock))
	defer mt.Close()

	mt.Run("success", func(mt *mtest.T) {
		mt.AddMockResponses(bson.D{
			{Key: "ok", Value: 0},
		})
		r := Repository{Collection: mt.Coll}
		err := r.Delete(primitive.NewObjectID())

		assert.Equal(t, 500, err.Code)
		assert.Equal(t, "Could not execute db query", err.Message)
	})
}
