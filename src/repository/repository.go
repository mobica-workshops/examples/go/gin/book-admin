package repository

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Repository struct {
	db         *mongo.Database
	Collection *mongo.Collection
}

type RequestError struct {
	Error   error
	Message string
	Code    int
}

func (r *Repository) RegisterIndex(name string, unique bool) string {
	log.Println("")
	indexName, err := r.Collection.Indexes().CreateOne(
		context.Background(),
		mongo.IndexModel{
			Keys:    bson.D{{Key: name, Value: 1}},
			Options: options.Index().SetUnique(unique),
		},
	)

	if err != nil {
		log.Fatal(err.Error())
	}

	return indexName
}
