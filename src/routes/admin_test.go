package routes

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestAddAdminRoutesGetAdmins(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedAdminService{}
	AddAdminRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/admins", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"get admins\"", w.Body.String())
}

func TestAddAdminRoutesGetAdmin(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedAdminService{}
	AddAdminRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/v1/admins/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"get admin\"", w.Body.String())
}

func TestAddAdminRoutesCreateAdmin(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedAdminService{}
	AddAdminRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/v1/admins", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"create admin\"", w.Body.String())
}

func TestAddAdminRoutesUpdateAdmin(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedAdminService{}
	AddAdminRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/v1/admins/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"update admin\"", w.Body.String())
}

func TestAddAdminRoutesDeleteAdmin(t *testing.T) {
	router := gin.Default()
	mainRoute := router.Group("/v1")
	service := MockedAdminService{}
	AddAdminRoutes(mainRoute, &service)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/v1/admins/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "\"delete admin\"", w.Body.String())
}
