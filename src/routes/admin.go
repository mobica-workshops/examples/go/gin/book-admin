package routes

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/services"

	"github.com/gin-gonic/gin"
)

func AddAdminRoutes(rg *gin.RouterGroup, service services.AdminService) {
	admins := rg.Group("/admins")

	admins.POST("", service.CreateAdmin)
	admins.GET("", service.GetAdmins)
	admins.GET("/:id", service.GetAdmin)
	admins.PUT("/:id", service.UpdateAdmin)
	admins.DELETE("/:id", service.DeleteAdmin)
}
