package routes

import (
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/services"

	"github.com/gin-gonic/gin"
)

func AddHealthRoutes(rg *gin.RouterGroup, service services.HealthService) {
	health := rg.Group("/health")

	health.GET("", service.CheckHealth)
}
