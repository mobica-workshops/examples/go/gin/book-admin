package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type MockedAdminService struct {
}

func (s *MockedAdminService) CreateAdmin(c *gin.Context) {
	c.JSON(http.StatusOK, "create admin")
}

func (s *MockedAdminService) UpdateAdmin(c *gin.Context) {
	c.JSON(http.StatusOK, "update admin")
}

func (s *MockedAdminService) DeleteAdmin(c *gin.Context) {
	c.JSON(http.StatusOK, "delete admin")
}

func (s *MockedAdminService) GetAdmins(c *gin.Context) {
	c.JSON(http.StatusOK, "get admins")
}

func (s *MockedAdminService) GetAdmin(c *gin.Context) {
	c.JSON(http.StatusOK, "get admin")
}

type MockedHealthService struct {
}

func (s *MockedHealthService) CheckHealth(c *gin.Context) {
	c.JSON(http.StatusOK, "check health")
}
