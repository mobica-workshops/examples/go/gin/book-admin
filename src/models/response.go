package models

type (
	Response struct {
		Status  int         `json:"status"`
		Message string      `json:"message"`
		Content interface{} `json:"content"`
	}

	Pagination struct {
		Count   int64       `json:"count"`
		Skip    int64         `json:"skip"`
		Limit   int64         `json:"limit"`
		Results interface{} `json:"results"`
	}

	NextPrevContent struct {
		Count    int64       `json:"count,omitempty"`
		Next     string      `json:"next,omitempty"`
		Previous string      `json:"previous,omitempty"`
		Results  interface{} `json:"results"`
	}

	ErrorResponse struct {
		Status  int      `json:"status"`
		Message string   `json:"message"`
		Errors  []string `json:"errors"`
	}

	NewAdminResponse struct {
		ID string `json:"id"`
	}
)

// NewResponse is the Response struct factory function.
func NewResponse(status int, message string, content interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: content,
	}
}

// NewNextPrevResponse will create http paginated response based on next and prev
func NewNextPrevResponse(status int, count int64, message, next, prev string, results interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: &NextPrevContent{
			Count:    count,
			Next:     next,
			Previous: prev,
			Results:  results,
		},
	}
}

func GenerateResponse(status int, message string, content interface{}) *Response {
	return &Response{
		Status:  status,
		Message: message,
		Content: content,
	}
}

func GenerateErrorResponse(status int, message string, errorMessages []string) *ErrorResponse {
	return &ErrorResponse{
		Status:  status,
		Message: message,
		Errors:  errorMessages,
	}
}
