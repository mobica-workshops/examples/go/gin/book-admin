package models

type PaginationInput struct {
	Limit  int64  `form:"limit,default=10"  binding:"gte=1"`
	Offset int64  `form:"skip,default=0"`
	Email  string `form:"email"`
}
