package models

import (
	"bytes"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
)

const (
	COST = 14
)

type (
	Admin struct {
		ID           primitive.ObjectID `json:"id" bson:"_id,omitempty"`
		Email        string             `json:"email" bson:"email,omitempty"`
		FirstName    string             `json:"firstName" bson:"firstName,omitempty"`
		LastName     string             `json:"lastName" bson:"lastName,omitempty"`
		PasswordHash string             `json:"passwordHash" bson:"passwordHash,omitempty"`
		Version      int                `json:"version" bson:"version,omitempty"`
	}

	CreateAdminInput struct {
		Email     string `json:"email" binding:"required,email"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		Password  string `json:"password" binding:"required"`
		Version   int    `json:"version"`
	}

	AdminID struct {
		ID string `uri:"id" binding:"required,len=24"`
	}

	UpdateAdminInput struct {
		Email        string `json:"email,omitempty"`
		FirstName    string `json:"firstName,omitempty"`
		LastName     string `json:"lastName,omitempty"`
		Password     string `json:"password,omitempty"`
		PasswordHash string `json:"-"`
		Version      int    `json:"version,omitempty"`
	}
)

func NewAdmin(input *CreateAdminInput) (*Admin, error) {
	hash, err := HashPassword(input.Password)

	if err != nil {
		return nil, err
	}

	version := 1

	if input.Version != 0 {
		version = input.Version
	}

	return &Admin{
		Email:        input.Email,
		FirstName:    input.FirstName,
		LastName:     input.LastName,
		PasswordHash: hash,
		Version:      version,
	}, nil
}

func HashPassword(password string) (string, error) {
	byteArray, err := bcrypt.GenerateFromPassword([]byte(password), COST)

	if err != nil {
		return "", err
	}

	return bytes.NewBuffer(byteArray).String(), nil
}
