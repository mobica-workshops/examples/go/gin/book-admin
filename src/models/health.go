package models

type Health struct {
	Alive bool `json:"alive"`
	Mongo bool `json:"mongo"`
}
