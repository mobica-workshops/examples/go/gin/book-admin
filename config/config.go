package config

import (
	"log"
	"net/url"

	"github.com/spf13/viper"
)

type MongoDbConfig struct {
	Srv                    bool   `mapstructure:"srv"`
	Host                   string `mapstructure:"host"`
	AuthenticationDatabase string `mapstructure:"authentication-database"`
	User                   string `mapstructure:"user"`
	Password               string `mapstructure:"password"`
	Database               string `mapstructure:"database"`
	ReplicaSet             string `mapstructure:"replica-set"`
	Uri                    string
}

type ServerConfig struct {
	Host string `mapstructure:"host"`
	Port string `mapstructure:"port"`
}

type Config struct {
	Environment string        `mapstructure:"env"`
	Server      ServerConfig  `mapstructure:"server"`
	MongoDb     MongoDbConfig `mapstructure:"mongodb"`
}

func GetConfiguration() (*Config, error) {
	var config *Config

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatal("Parsing configuration failed, err:", err)

		return config, err
	}

	config.setMongoUri()

	return config, nil
}

func (c *Config) setMongoUri() {
	var mongoScheme string

	if c.MongoDb.Srv {
		mongoScheme = "mongodb+srv"
	} else {
		mongoScheme = "mongodb"
	}
	uri := url.URL{
		Scheme: mongoScheme,
		User:   url.UserPassword(c.MongoDb.User, c.MongoDb.Password),
		Host:   c.MongoDb.Host,
		Path:   "/",
	}
	q := uri.Query()
	if c.MongoDb.AuthenticationDatabase != "" {
		q.Set("authSource", c.MongoDb.AuthenticationDatabase)
	}
	if c.MongoDb.ReplicaSet != "" {
		q.Set("replicaSet", c.MongoDb.ReplicaSet)
	}
	uri.RawQuery = q.Encode()
	c.MongoDb.Uri = uri.String()
}
