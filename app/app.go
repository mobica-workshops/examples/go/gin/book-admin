package app

import (
	"fmt"

	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/config"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/db"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/middleware"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/routes"
	"gitlab.com/mobica-workshops/examples/go/gin/book-admin/src/services"

	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo"
)

type App struct {
	AdminService  *services.Service
	HealthService *services.Service
	router        *gin.Engine
	Database      *mongo.Database
}

func RunApp(config *config.Config, seed bool) {
	if seed {
		db.Seed(config)
	}

	startServer(config)
}

func startServer(config *config.Config) {
	app := App{}
	app.initiateServer(config)
	app.registerAPI()

	defer app.closeServer()

	app.runServer(config.Server.Port)
}

func (app *App) initiateServer(config *config.Config) {
	app.Database = db.ConnectDB(config.MongoDb.Uri, config.MongoDb.Database)
	app.AdminService = services.NewAdminService(app.Database)
	app.HealthService = services.NewHealthService(app.Database)

	ginMode := getGinMode(config.Environment)
	gin.SetMode(ginMode)
	app.router = gin.Default()
}

func (app *App) registerAPI() {
	app.router.Use(middleware.CORSMiddleware())
	mainRoute := app.router.Group("/v1")

	routes.AddHealthRoutes(mainRoute, app.HealthService)
	routes.AddAdminRoutes(mainRoute, app.AdminService)
}

func (app *App) runServer(port string) {
	app.router.Run(fmt.Sprintf(":%s", port))
}

func (app *App) closeServer() {
	db.DisconnectDB(app.Database)
}

func getGinMode(serverMode string) string {
	mode := ""
	switch serverMode {
	case "prod":
		mode = gin.ReleaseMode
	case "dev":
		mode = gin.DebugMode
	default:
		mode = gin.TestMode
	}

	return mode
}
